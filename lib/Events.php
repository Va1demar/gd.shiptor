<?php

namespace AT\Shiptor;

use Bitrix\Main\Page\Asset;

class Events
{
    const MODULE_ID = 'at.shiptor';

    public function MyOnAdminListDisplay(&$list)
    {
        if ($list->table_id == 'tbl_sale_order') {
            $list->arActions['add_package'] = array(
                'type' => 'button',
                'name' => 'Отправить в курьерские службы',
                'action' => 'shiptorAddPackage.add(this.closest("form"))',
                'value' => 'add_package',
                'title' => 'Отправить в курьерские службы'
            );
            Asset::getInstance()->addJs('/local/modules/' . self::MODULE_ID . '/assets/js/add_package.js');
        }
    }
}
