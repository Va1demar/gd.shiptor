<?php

namespace AT\Shiptor\Handlers;

use Bitrix\Sale\Delivery\Services\Base;
use \Bitrix\Sale\Shipment;
use \Bitrix\Main\SystemException;

class DeliveryHandler extends Base
{
    protected static $canHasProfiles = true;

    public function __construct(array $initParams)
    {
        parent::__construct($initParams);
    }

    public static function getClassTitle()
    {
        return 'Агрегатор доставки Shiptor';
    }

    public static function getClassDescription()
    {
        return 'Агрегатор доставки Shiptor';
    }

    public static function canHasProfiles()
    {
        return self::$canHasProfiles;
    }

    public static function getChildrenClassNames()
    {
        return array('\AT\Shiptor\Handlers\ProfilesHandler');
    }

    public function getProfilesList()
    {
        return [
            'cdek_delivery_point' => 'CDEK ПВЗ',
            'boxberry_delivery_point' => 'Boxberry ПВЗ',
            'dpd_eparcel_delivery' => 'DPD ПВЗ',
            'pickpoint' => 'PickPoint',
            'iml_delivery' => 'IML ПВЗ',
            'shiptor_terminal' => 'Shiptor Самовывоз',
            'iml_courier' => 'IML Курьер',
            'boxberry_courier' => 'Boxberry Курьер',
            'cdek_courier' => 'CDEK Курьер',
            'dpd_eparcel_courier' => 'DPD Курьер Авто',
            'russian_post' => 'Shiptor Почта'
        ];
    }

    /*static function getPropertyByCode($propertyCollection, $code)
    {
        foreach ($propertyCollection as $property) {
            if ($property->getField('CODE') == $code)
                return $property;
        }
    }*/

    protected function calculateConcrete(Shipment $shipment)
    {
        throw new SystemException('Расчёт только через профили');
    }

    public function isCalculatePriceImmediately(): bool
    {
        return true;
    }

    public static function whetherAdminExtraServicesShow(): bool
    {
        return false;
    }

    protected function getConfigStructure(): array
    {
        /*$result = array(
            'MAIN' => array(
                'TITLE' => 'Дополнительные настройки',
                'DESCRIPTION' => 'Дополнительные настройки',
                'ITEMS' => array(
                    'TESTING' => array(
                        'TYPE' => 'Y/N',
                        'NAME' => 'Режим тестирования',
                    ),
                )
            )
        );
        return $result;*/
        return [];
    }
}
