<?php

namespace AT\Shiptor\Handlers;

use \Bitrix\Main\EventResult,
    \Bitrix\Main\Event;

class Handlers
{
    const MODULE_ID = "at.shiptor";

    public function addCustomDeliveryServices(Event $event)
    {
        $result = new EventResult(
            EventResult::SUCCESS,
            array(
                '\AT\Shiptor\Handlers\DeliveryHandler' => '/local/modules/' . self::MODULE_ID . '/lib/handlers/DeliveryHandler.php',
                '\AT\Shiptor\Handlers\ProfilesHandler' => '/local/modules/' . self::MODULE_ID . '/lib/handlers/ProfilesHandler.php',
            )
        );

        return $result;
    }
}
