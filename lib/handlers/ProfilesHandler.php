<?php

namespace AT\Shiptor\Handlers;

use Bitrix\Sale\Delivery\CalculationResult,
    Bitrix\Sale\Delivery\Services\Manager,
    Bitrix\Sale\Delivery\Services\Base,
    Bitrix\Sale\Shipment,
    Bitrix\Main\Error,
    AT\Shiptor\CalculateShipping,
    AT\Shiptor\Helper;

class ProfilesHandler extends Base
{
    protected static $isProfile = true;
    protected static $parent = null;
    protected static $profileId = null;

    public function __construct(array $initParams)
    {
        $this->profileId = $initParams['PROFILE_ID'] ?: $initParams['CONFIG']['SHIPTOR']['CODE'];

        $initParams['NAME'] = $initParams['NAME'] ?: $this->getDefaultSetting()['NAME'];
        $initParams['CODE'] = $initParams['CODE'] ?: $this->getDefaultSetting()['CODE'];

        parent::__construct($initParams);
        $this->parent = Manager::getObjectById($this->parentId);
    }

    public static function getClassTitle()
    {
        return 'Агрегатор доставки Shiptor';
    }

    public static function getClassDescription()
    {
        return 'Агрегатор доставки Shiptor';
    }

    /*static function getPropertyByCode($propertyCollection, $code)
    {
        foreach ($propertyCollection as $property) {
            if ($property->getField('CODE') == $code)
                return $property;
        }
    }*/

    /*private function __calculateServiceType()
    {
    }*/

    protected function calculateConcrete(Shipment $shipment = null)
    {
        $result = new CalculationResult();

        $weight = number_format((floatval($shipment->getWeight()) / 1000), 2);

        if ($weight <= 0) {
            $result->addError(new Error('Нулевой вес заказа'));
            return $result;
        }

        $order = $shipment->getCollection()->getOrder();

        $props = $order->getPropertyCollection();

        $orderPrice = $order->getPrice() - $order->getDeliveryPrice();
        $kladr = $props->getItemByOrderPropertyCode('ADDRESS_KLADR')->getValue();

        if (!$kladr || empty($kladr)) {
            $result->addError(new Error('Не найден кладр заказа'));
            return $result;
        }

        $dimensionsPackage = Helper::getSidePackage($order, [$orderPrice, $weight, $kladr]);

        $shiptor = new CalculateShipping();
        $arMethods = $shiptor->getAvailableMethods($weight, $orderPrice, $this->config['SHIPTOR']['SHIPTOR_CODE_COURIER'], $kladr, $dimensionsPackage);
        if (empty($arMethods['result']['methods'])) {
            $result->addError(new Error('Данный способ (' . $this->name . ') доставки недоступен'));
            return $result;
        }

        $arMethod = $shiptor->getMethod($arMethods, $this->config['SHIPTOR']['SHIPTOR_CODE_METHOD']);
        if (!$arMethod) {
            $result->addError(new Error('Данный способ (' . $this->name . ') доставки недоступен'));
            return $result;
        }

        $deliveryPrice = $shiptor->getPriceDelivery($arMethod);
        if ($deliveryPrice <= 0) {
            $result->addError(new Error('Для доставки (' . $this->name . ') произошел нулевой расчёт'));
            return $result;
        }

        $result->setDeliveryPrice($deliveryPrice);
        //$result->setPeriodDescription(''); //здесь сожно задать сроки доставки

        return $result;
    }

    public function isCalculatePriceImmediately()
    {
        return $this->getParentService()->isCalculatePriceImmediately();
    }

    public static function whetherAdminExtraServicesShow()
    {
        return true;
    }

    protected function getConfigStructure()
    {
        $result = [
            'SHIPTOR' => [
                'TITLE' => 'Настройки Shiptor',
                'DESCRIPTION' => 'Настройки Shiptor',
                'ITEMS' => [
                    'SHIPTOR_ID_DELIVERY' => [
                        'TYPE' => 'NUMBER',
                        'NAME' => 'ID доставки в Shiptor',
                        'VALUE' => $this->getDefaultSetting()['ID'],
                        'DEFAULT' => $this->getDefaultSetting()['ID'],
                        //'READONLY' => true
                    ],
                    'SHIPTOR_CODE_COURIER' => [
                        'TYPE' => 'STRING',
                        'NAME' => 'Код курьерской службы',
                        'VALUE' => $this->getDefaultSetting()['COURIER'],
                        'DEFAULT' => $this->getDefaultSetting()['COURIER'],
                        //'READONLY' => true
                    ],
                    'SHIPTOR_CODE_METHOD' => [
                        'TYPE' => 'STRING',
                        'NAME' => 'Код метода доставки',
                        'VALUE' => $this->getDefaultSetting()['CODE'],
                        'DEFAULT' => $this->getDefaultSetting()['CODE'],
                        //'READONLY' => true
                    ],
                    'DELIVERY_POINT' => [
                        'TYPE' => 'Y/N',
                        'NAME' => 'Пункт выдачи',
                        //'VALUE' => $this->getDefaultSetting()['DELIVERY_POINT'] == 'Y' ? 'Y' : 'N',
                        //'DEFAULT' => $this->getDefaultSetting()['DELIVERY_POINT'] == 'Y' ? 'Y' : 'N',
                        //'READONLY' => true
                    ],
                    'TESTING' => [
                        'TYPE' => 'Y/N',
                        'NAME' => 'Режим тестирования',
                    ],
                ]
            ]
        ];
        return $result;
    }

    public function getParentService()
    {
        return $this->parent;
    }

    public static function isProfile()
    {
        return self::$isProfile;
    }

    public function isCompatible(Shipment $shipment)
    {
        $calcResult = self::calculateConcrete($shipment);
        return $calcResult->isSuccess();
    }

    private function getDefaultSetting()
    {
        $result = [
            'cdek_delivery_point' => [
                'NAME' => 'CDEK ПВЗ',
                'CODE' => 'cdek_delivery_point',
                'COURIER' => 'cdek',
                'DELIVERY_POINT' => 'Y',
                'ID' => 25
            ],
            'boxberry_delivery_point' => [
                'NAME' => 'Boxberry ПВЗ',
                'CODE' => 'boxberry_delivery_point',
                'COURIER' => 'boxberry',
                'DELIVERY_POINT' => 'Y',
                'ID' => 14
            ],
            'dpd_eparcel_delivery' => [
                'NAME' => 'DPD ПВЗ',
                'CODE' => 'dpd_eparcel_delivery',
                'COURIER' => 'dpd',
                'DELIVERY_POINT' => 'Y',
                'ID' => 18
            ],
            'pickpoint' => [
                'NAME' => 'PickPoint',
                'CODE' => 'pickpoint',
                'COURIER' => 'pickpoint',
                'DELIVERY_POINT' => 'Y',
                'ID' => 11
            ],
            'iml_delivery' => [
                'NAME' => 'IML ПВЗ',
                'CODE' => 'iml_delivery',
                'COURIER' => 'iml',
                'DELIVERY_POINT' => 'Y',
                'ID' => 53
            ],
            'shiptor_terminal' => [
                'NAME' => 'Shiptor Самовывоз',
                'CODE' => 'shiptor_terminal',
                'COURIER' => 'shiptor',
                'DELIVERY_POINT' => 'Y',
                'ID' => 35
            ],
            'iml_courier' => [
                'NAME' => 'IML Курьер',
                'CODE' => 'iml_courier',
                'COURIER' => 'iml',
                'DELIVERY_POINT' => 'N',
                'ID' => 19
            ],
            'boxberry_courier' => [
                'NAME' => 'Boxberry Курьер',
                'CODE' => 'boxberry_courier',
                'COURIER' => 'boxberry',
                'DELIVERY_POINT' => 'N',
                'ID' => 13
            ],
            'cdek_courier' => [
                'NAME' => 'CDEK Курьер',
                'CODE' => 'cdek_courier',
                'COURIER' => 'cdek',
                'DELIVERY_POINT' => 'N',
                'ID' => 24
            ],
            'dpd_eparcel_courier' => [
                'NAME' => 'DPD Курьер Авто',
                'CODE' => 'dpd_eparcel_courier',
                'COURIER' => 'dpd',
                'DELIVERY_POINT' => 'N',
                'ID' => 17
            ],
            'russian_post' => [
                'NAME' => 'Shiptor Почта',
                'CODE' => 'russian_post',
                'COURIER' => 'russian-post',
                'DELIVERY_POINT' => 'Y',
                'ID' => 20
            ],
        ];

        return $result[$this->profileId];
    }
}
