<?php

namespace AT\Shiptor;

use AT\Shiptor\Shiptor,
    Bitrix\Main\Data\Cache;

class DeliveryPoints extends Shiptor
{
    const CACHE_TIME = 600;
    const MAX_DP = 1000; //максимальное количество ПВЗ за один запрос

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Получить ПВЗ
     *
     * @param  float $weight Вес заказа
     * @param  string $courier Код курьерской службы
     * @param  string $kladr Кладр адреса
     * @param  float $side Размер коробки отправления
     * @param  int $page Номер страницы при запросе
     * @return array
     */
    public function getDeliveryPoints(
        float $weight,
        string $courier,
        string $kladr,
        array $dimensionsPackage = ['length' => 20, 'width' => 20, 'height' => 20],
        int $page = 1
    ): array {
        $arRes = [];

        $cache = Cache::createInstance();
        $cacheKey = implode('_', [$weight, $courier, $kladr]);
        if ($cache->initCache(self::CACHE_TIME, $cacheKey, '/shiptor_results/dp/')) {
            $arRes = $cache->getVars();
        } elseif ($cache->startDataCache()) {
            $url = 'shipping/v2';
            $method = 'getDeliveryPoints';
            $arParams = [
                'page' => $page,
                'per_page' => self::MAX_DP,
                'kladr_id' => $kladr,
                'courier' => $courier,
                'limits' => [
                    'weight' => $weight,
                    'length' => $dimensionsPackage['length'],
                    'width' => $dimensionsPackage['width'],
                    'height' => $dimensionsPackage['height'],
                ],
            ];
            $arRes = $this->__connect($url, $method, $arParams);
            $cache->endDataCache($arRes);
        }

        return $arRes;
    }
}
