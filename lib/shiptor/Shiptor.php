<?php

namespace AT\Shiptor;

use Bitrix\Main\Config\Option,
    Bitrix\Main\Web\HttpClient,
    Bitrix\Main\SystemException;

class Shiptor
{
    const MODULE_ID = 'at.shiptor';

    protected $token = null;
    protected $url = null;

    public function __construct()
    {
        $this->token = Option::get(self::MODULE_ID, 'KEY_PARTNER');
        $this->url = Option::get(self::MODULE_ID, 'URL_API');
    }
    
    /**
     * Запрос на Shiptor
     *
     * @param  string $url
     * @param  string $method
     * @param  array $arParams
     * @return array|string
     */
    protected function __connect(string $url, string $method, array $arParams)
    {
        if (!$this->token || empty($this->token) || !$this->url || empty($this->url)) {
            throw new SystemException('Заполните настройки модуля: токен и url');
        }

        $httpClient = new HttpClient();
        $httpClient->setHeader('Content-Type', 'application/json', true);
        $httpClient->setHeader('x-authorization-token', $this->token, false);

        $query = $this->getHeadQuery();
        $query['method'] = $method;
        $query['params'] = $arParams;

        $httpClient->query('POST', $this->url . $url, json_encode($query, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));

        if ($httpClient->getStatus() != 200) {
            throw new SystemException('Неправильный ответ сервера');
        }

        return json_decode($httpClient->getResult(), true);
    }

    private function getHeadQuery(): array
    {
        return [
            'id' => 'JsonRpcClient.js',
            'jsonrpc' => '2.0',
        ];
    }

    /**
     * Массив ID доставок Shiptor, указанных в настройках модуля
     *
     * @return array
     */
    protected function getIdsDeliveryShiptor()
    {
        return explode(',', str_replace(' ', '', Option::get(self::MODULE_ID, 'IDS_DELIVERY')));
    }
}
