<?php

namespace AT\Shiptor;

use AT\Shiptor\Shiptor,
    AT\Shiptor\Helper,
    Bitrix\Main\Loader,
    Bitrix\Sale,
    Bitrix\Sale\Delivery,
    Bitrix\Main\UserTable,
    Bitrix\Main\Config\Option,
    OL\Profile\Internals\OrderAddressesTable;

class AddPackage extends Shiptor
{
    const MODULE_ID = 'at.shiptor';
    const STATUS_OK_FOR_SHIPMENT = 'DF';

    private $isTest = false;
    private $order = null;
    private $orderID = null;
    private $basket = null;
    private $isDeliveryPoint = false;
    private $deliveryId = null;

    public function __construct(int $orderId)
    {
        Loader::includeModule("sale");
        $this->order = Sale\Order::load($orderId);
        $this->orderID = $this->order->getId();
        $this->basket = $this->order->getBasket();
        $this->deliveryId = $this->getDeliveryId();
        parent::__construct();
    }

    public function add(array $arData)
    {
        $error = $this->checkError($arData);

        if ($error) {
            $this->setErrorStatus();
            return $this->orderID . ': Ошибка - ' . $error;
        }

        $url = 'shipping/v1';
        $method = 'addPackage';

        $arParams = [
            "stock" => 1,
            'length' => $arData['length'],
            'width' => $arData['width'],
            'height' => $arData['height'],
            'weight' => $arData['weight'],
            'cod' => 0,
            "declared_cost" => $arData['price'],
            "external_id" => $arData['order_id'],
            "departure" => [
                "shipping_method" => $arData['shiptor_delivery_id'],
                "cashless_payment" => false,
                "address" => [
                    "country" => "RU",
                    "receiver" => $arData['person']['fullname'],
                    "name" => $arData['person']['name'],
                    "surname" => $arData['person']['surname'],
                    "email" => $arData['person']['email'],
                    "phone" => $arData['person']['phone'],
                    "postal_code" => $arData['address']['index'],
                    "administrative_area" => '',
                    "settlement" => '',
                    "street" => $arData['address']['street'],
                    "house" => $arData['address']['house'],
                    "apartment" => $arData['address']['flat'],
                    "address_line_1" => $arData['address']['full_address'],
                    "kladr_id" => $arData['address']['kladr']
                ]
            ],
            "products" => [],
            "services" => [],
        ];

        if ($this->isDeliveryPoint) {
            $arParams['departure']['delivery_point'] = $arData['shiptor_delivery_point'];
        }

        foreach ($arData['products'] as $arProduct) {
            $arParams['products'][] = [
                'shopArticle' => $arProduct['name'],
                'name' => $arProduct['name'],
                'count' => $arProduct['quantity'],
                'price' => $arProduct['price'],
                'vat' => null
            ];
        }

        if ($this->isTest()) {
            $arParams['no_gather'] = true;
        }

        //TODO ручной ответ для теста
        //$arRes = $this->__connect($url, $method, $arParams);
        $arRes = [
            'result' => [
                'id' => 456,
            ]
        ];

        $this->saveResult($arRes);

        if (!empty($arRes['error'])) {
            $this->setErrorStatus();
            return $this->orderID . ': Ошибка - ' . $arRes['error']['message'];
        }

        if ((int)$arRes['result']['id'] > 0) {
            $this->setSuccessStatus();
            return $this->orderID . ': Успешно';
        }
    }

    public function getOrderData()
    {
        $order = $this->order;
        $basket = $this->basket;
        $dimensionsPackage = Helper::getSidePackage($order);
        $arRes = [
            'order_id' => (int)$this->orderID,
            'price' => (float)$basket->getPrice(),
            'length' => (float)$dimensionsPackage['length'],
            'width' => (float)$dimensionsPackage['width'],
            'height' => (float)$dimensionsPackage['height'],
        ];

        $propertyCollection = $order->getPropertyCollection();
        $adressPropValue = $propertyCollection->getItemByOrderPropertyCode('ADDRESS');
        $arAddress = $this->getAddress($adressPropValue->getValue());
        $arRes['address'] = [
            'index' => (string)$arAddress['ADDRESS_INDEX'],
            'street' => (string)$arAddress['ADDRESS_STREET'],
            'house' => (string)$arAddress['ADDRESS_HOUSE'],
            'flat' => !empty($arAddress['ADDRESS_FLAT']) ? (string)$arAddress['ADDRESS_FLAT'] : '',
            'full_address' => (string)$arAddress['ADDRESS_STRING'],
            'kladr' => (string)$arAddress['ADDRESS_KLADR'],
        ];

        $arDelivery = $this->getDelivery($this->deliveryId);
        $arRes['shiptor_delivery_id'] = (int)$arDelivery['CONFIG']['SHIPTOR']['SHIPTOR_ID_DELIVERY'];
        $this->isDeliveryPoint = $arDelivery['CONFIG']['SHIPTOR']['DELIVERY_POINT'] == 'Y';

        if ($this->isDeliveryPoint) {
            $arRes['shiptor_delivery_point'] = $propertyCollection->getItemByOrderPropertyCode('ID_DELIVERY_POINT')->getValue();
        }

        $this->isTest = $arDelivery['CONFIG']['SHIPTOR']['TESTING'] !== 'N';

        if ($this->isTest) {
            $arRes['person'] = $this->getTestPerson();
        } else {
            $userId = $order->getField("USER_ID");
            $arUserInfo = $this->getUserInfo($userId);
            $arRes['person'] = [
                'name' => $arUserInfo['NAME'],
                'surname' => $arUserInfo['LAST_NAME'],
                'fullname' => $arUserInfo['LAST_NAME'] . ' ' . $arUserInfo['NAME'],
                'email' => $arUserInfo['EMAIL'],
                'phone' => !empty($arUserInfo['PERSONAL_PHONE']) ? $arUserInfo['PERSONAL_PHONE'] : $arUserInfo['PERSONAL_MOBILE'],
            ];

            $recipientFullName = $adressPropValue = $propertyCollection->getItemByOrderPropertyCode('PERSON_NAME')->getValue();
            if ($recipientFullName && !empty($recipientFullName)) {
                $arRes['person']['fullname'] = $recipientFullName;
                $arRecipientFullName = explode(' ', $recipientFullName);
                $arRes['person']['name'] = $arRecipientFullName[0];
                $arRes['person']['surname'] = $arRecipientFullName[1];
            }

            $recipientPhone = $adressPropValue = $propertyCollection->getItemByOrderPropertyCode('PERSON_PHONE')->getValue();
            if ($recipientPhone && !empty($recipientPhone)) {
                $arRes['person']['phone'] = $recipientPhone;
            }
        }

        $arProducts = $this->getProducts($basket);
        $arRes['products'] = $arProducts;

        $arRes['weight'] = (float)$this->getWeightOrder($basket);

        return $arRes;
    }

    private function getUserInfo(int $userId): array
    {
        $arUser = UserTable::getList(array(
            'select' => array('*'),
            'filter' => array('ID' => $userId)
        ))->Fetch();

        return $arUser;
    }

    private function getOrder(): Sale\Order
    {
        return $this->order;
    }

    private function getAddress(int $id): array
    {
        if (Loader::includeModule("olaplex.profile")) {
            return OrderAddressesTable::getList([
                'select' => ['*'],
                'filter' => [
                    'ID' => $id
                ]
            ])->Fetch();
        }
        return [];
    }

    private function getDelivery(int $id): array
    {
        return Delivery\Services\Table::getList(array(
            'filter' => [
                'id' => $id
            ],
            'cache' => ['ttl' => 5]
        ))->Fetch();
    }

    private function getBasket(): Sale\Basket
    {
        return $this->basket;
    }

    private function getProducts(Sale\Basket $basket): array
    {
        $arRes = [];
        $basketItems = $basket->getBasketItems();
        foreach ($basketItems as $basketItem) {
            $arRes[] = [
                'article' => $basketItem->getField('NAME'),
                'name' => $basketItem->getField('NAME'),
                'quantity' => $basketItem->getQuantity(),
                'price' => $basketItem->getPrice(),
            ];
        }
        return $arRes;
    }

    private function getWeightOrder(Sale\Basket $basket)
    {
        return number_format($basket->getWeight() / 1000, 2, '.', '');
    }

    private function getTestPerson(): ?array
    {
        return [
            'name' => 'ТЕСТ',
            'surname' => 'ТЕСТ',
            'fullname' => 'ТЕСТ ТЕСТ',
            'email' => 'test@test.ru',
            'phone' => '+78005553535',
        ];
    }

    public function isTest()
    {
        return $this->isTest;
    }

    public function checkError(array $arData): ?string
    {
        //проверить, есть ли уже такой заказ в лк
        if ($this->isDouble($arData['order_id'])) {
            return 'Этот заказ уже есть в ЛК Shiptor';
        }

        if (empty($arData['price']) || $arData['price'] <= 0) {
            return 'Не указана цена';
        }

        if (empty($arData['length']) || empty($arData['width']) || empty($arData['height'])) {
            return 'Не указан размер отправления (коробки)';
        }

        if (empty($arData['address'])) {
            return 'Не указан адрес';
        }

        foreach ($arData['address'] as $code => $value) {
            if (empty($value)) {
                switch ($code) {
                    case 'index':
                        return 'Не указан индекс';
                    case 'street':
                        return 'Не указана улица';
                    case 'house':
                        return 'Не указан дом';
                    case 'full_address':
                        return 'Не указан полный адрес в строку';
                    case 'kladr':
                        return 'Не указан КЛАДР';
                    default:
                        break;
                }
            }
        }

        if (empty($arData['shiptor_delivery_id'])) {
            return 'Не задан Shiptor Delivery ID (код службы доставки)';
        }

        if ($this->isDeliveryPoint && (empty($arData['shiptor_delivery_point']) || $arData['shiptor_delivery_point'] <= 0)) {
            return 'Не задан ID ПВЗ';
        }

        if (empty($arData['person'])) {
            return 'Не задан получатель';
        }

        foreach ($arData['person'] as $code => $value) {
            if (empty($value)) {
                switch ($code) {
                    case 'name':
                        return 'Не указан индекс';
                    case 'surname':
                        return 'Не указана фамилия';
                    case 'fullname':
                        return 'Не указаны имя и фамилия';
                    case 'email':
                        return 'Не указан email';
                    case 'phone':
                        return 'Не указан номер телефона';
                    default:
                        break;
                }
            }
        }

        if (empty($arData['weight']) || $arData['weight'] <= 0) {
            return 'Не задан вес или равняется нулю';
        }

        if (empty($arData['products'])) {
            return 'В заказе нет товаров';
        }

        return null;
    }

    /**
     * Проверка ID сопособа доставки
     *
     * @return null|string
     */
    public function checkDelivery(): ?string
    {
        if (!in_array($this->getParentDelivery(), $this->getIdsDeliveryShiptor())) {
            return $this->orderID . ': Ошибка - Неверная служба доставки';
        }
        return null;
    }

    /**
     * ID доставки
     *
     * @return int
     */
    public function getDeliveryId()
    {
        return $this->order->getField("DELIVERY_ID");
    }

    /**
     * ID родительской доставки
     *
     * @return void
     */
    private function getParentDelivery()
    {
        $arDelivery = $this->getDelivery($this->deliveryId);
        return $arDelivery['PARENT_ID'];
    }

    /**
     * Проверка оплаты заказа
     *
     * @return null|string
     */
    public function checkPayment(): ?string
    {
        if (Option::get(self::MODULE_ID, 'CHECK_PAY_OK') == 'Y') {
            if ($this->order->getField("PAYED") == 'N') {
                return $this->orderID . ': Ошибка - Заказ не оплачен';
            }
        }
        return null;
    }

    /**
     * Проверка статуса заказа
     *
     * @return null|string
     */
    public function checkCorrectStatus(): ?string
    {
        $statusOrder = $this->order->getField("STATUS_ID");
        if (in_array($statusOrder, $this->getArrCorrectStatuses())) {
            return null;
        }
        return $this->orderID . ': Ошибка - Некорректный статус заказа';
    }

    private function getArrCorrectStatuses(): array
    {
        return explode(',', str_replace(' ', '', Option::get(self::MODULE_ID, 'STATUSES_ORDER')));
    }

    /**
     * Успешный статус для отгрузки после отправки в КС
     *
     * @return void
     */
    public function setSuccessStatus(): void
    {
        $shipmentCollection = $this->order->getShipmentCollection();
        foreach ($shipmentCollection as $shipment) {
            if ($shipment->isSystem())
                continue;
            $shipment->setField('STATUS_ID', self::STATUS_OK_FOR_SHIPMENT);
        }
        $this->order->save();
    }

    /**
     * Статус с ошибкой при попытке отправки в КС
     *
     * @return void
     */
    private function setErrorStatus(): void
    {
        return;
        /*$this->order->setField('STATUS_ID', 'ED');
        $this->order->save();*/
    }

    /**
     * Сохранить результат выполнения
     *
     * @param  mixed $arRes
     * @return void
     */
    private function saveResult(array $arRes): void
    {
        $arRes['ORDER_ID'] = $this->orderID;
        $arRes['DATE_TIME'] = date('d.m.y H:i:s');
        $dir = $_SERVER['DOCUMENT_ROOT'] . '/upload/' . self::MODULE_ID . '/' . date('d_m_y') . '/';
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }
        $filename = $this->orderID . '__' . date('H_i_s') . '.txt';
        file_put_contents($dir . $filename, print_r($arRes, true));
    }

    private function isDouble(int $orderId)
    {
        $url = 'shipping/v1';
        $method = 'getPackage';
        $arParams['external_id'] = $orderId;
        $arRes = $this->__connect($url, $method, $arParams);
        if (isset($arRes['result']['id']) && empty($arRes['error'])) {
            return true;
        }
        return false;
    }
}
