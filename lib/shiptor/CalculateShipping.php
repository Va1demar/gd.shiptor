<?php

namespace AT\Shiptor;

use AT\Shiptor\Shiptor,
    Bitrix\Main\Data\Cache;

class CalculateShipping extends Shiptor
{
    const CACHE_TIME = 600;

    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Получить доступные способы доставки для конкретной курьерской службы
     *
     * @param  float $weight Вес заказа
     * @param  float $price Цена заказа
     * @param  string $courier Символьный код курьерской службы
     * @param  string $kladr Код КЛАДР адреса доставки
     * @param  array $side Габариты отправления
     * @return array
     */
    public function getAvailableMethods(float $weight, float $price, string $courier, string $kladr, array $side = ['length' => 20, 'width' => 20, 'height' => 20]): array
    {
        $arRes = [];

        $cache = Cache::createInstance();
        $cacheKey = implode('_', [$weight, $price, $courier, $kladr]);
        if ($cache->initCache(self::CACHE_TIME, $cacheKey, '/shiptor_results/')) {
            $arRes = $cache->getVars();
        } elseif ($cache->startDataCache()) {
            $url = 'shipping/v1';
            $method = 'calculateShipping';
            $arParams = [
                'stock' => false,
                'length' => $side['length'],
                'width' => $side['width'],
                'height' => $side['height'],
                'weight' => $weight,
                'cod' => 0,
                'declared_cost' => $price,
                'courier' => $courier,
                'kladr_id' => $kladr,
            ];
            $arRes = $this->__connect($url, $method, $arParams);
            $cache->endDataCache($arRes);
        }

        return $arRes;
    }
    
    /**
     * Получить конкретный способ доставки (курьер или ПВЗ) для курьерской службы
     *
     * @param  array $arMethods
     * @param  string $methodCode
     * @return array
     */
    public function getMethod(array $arMethods, string $methodCode): ?array
    {
        foreach ($arMethods['result']['methods'] as $arMethod) {
            if ($arMethod['method']['group'] == $methodCode) {
                return $arMethod;
            }
        }
        return null;
    }
    
    /**
     * Получить полную цену доставки
     *
     * @param  array $arMethod
     * @return float
     */
    public function getPriceDelivery(array $arMethod): float
    {
        return $arMethod['cost']['total']['sum'];
    }
}
