<?php

namespace AT\Shiptor;

use AT\Shiptor\Shiptor,
    Bitrix\Sale\Order,
    Bitrix\Sale\Delivery,
    Bitrix\Main\Loader;

class StatusPackage extends Shiptor
{
    const NEED_STATUS = 'N';

    public function __construct()
    {
        Loader::includeModule("sale");
        parent::__construct();
    }

    /**
     * Метод для агента
     *
     * @return void
     */
    public static function checkStatusAgent()
    {
        $status = new StatusPackage();
        $arOrders = $status->buildOrders();
        $status->checkStatus($arOrders);
        return "AT\Shiptor\StatusPackage::checkStatusAgent();";
    }

    /**
     * ID всех подходящих заказов
     *
     * @return array
     */
    public function buildOrders(): array
    {
        $arDelivery = $this->getIdsDelivery();
        $arOrders = [];
        if (!empty($arDelivery)) {
            $dbRes = Order::getList([
                'select' => ['ID'],
                'filter' => [
                    'DELIVERY_ID' => $arDelivery,
                    'STATUS_ID' => self::NEED_STATUS,
                    'PAYED' => 'Y'
                ]
            ]);
            while ($order = $dbRes->fetch()) {
                $arOrders[] = $order['ID'];
            }
        }
        return $arOrders;
    }

    /**
     * ID всех подходящих доставок
     *
     * @return array
     */
    private function getIdsDelivery(): array
    {
        $arRes = [];
        $arIdsDelShiptor = $this->getIdsDeliveryShiptor();
        if (!empty($arIdsDelShiptor)) {
            $dbRes = Delivery\Services\Table::getList([
                'select' => ['ID'],
                'filter' => [
                    'ACTIVE' => 'Y',
                    'PARENT_ID' => $arIdsDelShiptor
                ],
            ]);
            while ($delivery = $dbRes->fetch()) {
                $arRes[] = $delivery['ID'];
            }
        }
        return $arRes;
    }

    /**
     * Проверка статуса заказа в КС
     *
     * @param  array $arOrders
     * @return void
     */
    public function checkStatus(array $arOrders): void
    {
        $url = 'shipping/v1';
        $method = 'getPackage';
        foreach ($arOrders as $orderId) {
            $arParams['external_id'] = $orderId;
            $arRes = $this->__connect($url, $method, $arParams);
            if ((int)$arRes['result']['id'] > 0) {
                if ($arRes['result']['status'] == 'delivered') {
                    $orderObj = $this->getOrder($orderId);
                    $this->setSuccessStatus($orderObj);
                }
            } else {
                //$this->logWrite($orderId);
            }
        }
    }

    /**
     * Получить объект заказа
     *
     * @param  int $orderId
     * @return Order
     */
    private function getOrder(int $orderId): Order
    {
        return Order::load($orderId);
    }

    /**
     * Успешный статус после доставки товара клиенту
     *
     * @return void
     */
    private function setSuccessStatus($order): void
    {
        $order->setField('STATUS_ID', 'F');
        $order->save();
    }

    /**
     * Записать в логи, что невозможно получить статус заказа
     *
     * @return void
     */
    private function logWrite($orderId): void
    {
        $dir = $_SERVER['DOCUMENT_ROOT'] . '/upload/' . self::MODULE_ID . '/error_status/';
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }
        $filename = date('d_m_y') . '.txt';
        file_put_contents($dir . $filename, $orderId . ' - невозможно получить статус заказа (' . date('d.m.y H:i:s') . ')' . PHP_EOL, FILE_APPEND);
    }
}
