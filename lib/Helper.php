<?php

namespace AT\Shiptor;

use Bitrix\Sale,
    Bitrix\Sale\Delivery,
    Bitrix\Main\Data\Cache;

class Helper
{
    const DEFAULT_SIDE = 20;

    public static function getSidePackage(Sale\Order $order, $arCache = []): array
    {
        $cache = Cache::createInstance();
        $cacheKey = '';
        if (!empty($arCache)) {
            $cacheTime = 600;
            foreach ($arCache as $cacheOneKey) {
                $cacheKey .= $cacheOneKey . '_';
            }
        } else {
            $cacheTime = 0;
        }

        if ($cache->initCache($cacheTime, $cacheKey, '/at.shiptor/calcPackage/')) {
            $arRes = $cache->getVars();
        } elseif ($cache->startDataCache()) {
            $summV = 0;
            $basket = $order->getBasket();
            $basketItems = $basket->getBasketItems();
            $maxSide = 0;
            foreach ($basketItems as $basketItem) {
                $arDimensions = unserialize($basketItem->getField('DIMENSIONS'));

                if (!empty($arDimensions['WIDTH'])) {
                    $width = $arDimensions['WIDTH'] / 10;
                } else {
                    $width = self::DEFAULT_SIDE;
                }
                $maxSide = $width > $maxSide ? $width : $maxSide;

                if (!empty($arDimensions['HEIGHT'])) {
                    $height = $arDimensions['HEIGHT'] / 10;
                } else {
                    $height = self::DEFAULT_SIDE;
                }
                $maxSide = $height > $maxSide ? $height : $maxSide;

                if (!empty($arDimensions['LENGTH'])) {
                    $length = $arDimensions['LENGTH'] / 10;
                } else {
                    $length = self::DEFAULT_SIDE;
                }
                $maxSide = $length > $maxSide ? $length : $maxSide;

                $summV += ($width * $height * $length) * $basketItem->getQuantity();
            }

            $summV13 = ceil(pow($summV, 1 / 3));
            if ($maxSide > $summV13) {
                $arRes = [
                    'length' => ceil($maxSide),
                    'width' => ceil(pow(($summV / $maxSide), 1 / 2)),
                    'height' => ceil(pow(($summV / $maxSide), 1 / 2)),
                ];
            } else {
                $arRes = [
                    'length' => $summV13,
                    'width' => $summV13,
                    'height' => $summV13,
                ];
            }
            $cache->endDataCache($arRes);
        }
        return $arRes;
    }

    /**
     * Вернуть информацию по доставке
     *
     * @param  int $deliveryID
     * @return array
     */
    public static function getInfoDelivery(int $deliveryID): array
    {
        $arRes = [];
        if ($deliveryID > 0) {
            $arRes = Delivery\Services\Table::getList([
                'select' => ['*'],
                'filter' => [
                    'ID' => $deliveryID,
                ],
                'cache' => ['ttl' => (60 * 60 * 2)]
            ])->fetch();
        }
        return $arRes;
    }

    public static function createJsonForMap(array $data): string
    {
        $arRes['type'] = 'FeatureCollection';
        if (!empty($data['result']['items'])) {
            foreach ($data['result']['items'] as $arItem) {
                $arRes['features'][] = [
                    'type' => 'Feature',
                    'id' => (int)$arItem['id'],
                    'geometry' => [
                        'type' => 'Point',
                        'coordinates' => [
                            $arItem['gps_location']['latitude'],
                            $arItem['gps_location']['longitude'],
                        ],
                    ],
                    'properties' => [
                        'balloonContentHeader' => $arItem['name'],
                        'balloonContentBody' => $arItem['address'],
                        'balloonContentFooter' => '<button class="choose-dp" onclick="mapCheckout.chooseDP(this, ' . $arItem['id'] . ', \'' . $arItem['address'] . '\');return false;">Выбрать</button>',
                        'clusterCaption' => $arItem['name'],
                    ],
                ];
            }
        }
        return json_encode($arRes, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
    }
}
