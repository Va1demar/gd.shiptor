<?

use Bitrix\Main\Localization\Loc,
    Bitrix\Main\ModuleManager,
    Bitrix\Main\EventManager;

Loc::loadMessages(__FILE__);

class at_shiptor extends CModule
{

    var $MODULE_ID = "at.shiptor";
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $PARTNER_NAME;
    var $PARTNER_URI;
    var $errors;

    function __construct()
    {
        $arModuleVersion = array();
        $path = str_replace("\\", "/", __FILE__);
        $path = substr($path, 0, strlen($path) - strlen("/index.php"));
        include($path . "/version.php");
        if (is_array($arModuleVersion) && isset($arModuleVersion["VERSION"])) {
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        }
        $this->MODULE_NAME = Loc::getMessage("SHIPTOR_INSTALL_NAME");
        $this->MODULE_DESCRIPTION = Loc::getMessage("SHIPTOR_INSTALL_DESC");
        $this->PARTNER_NAME = Loc::getMessage("PARTNER_NAME");
        $this->PARTNER_URI = Loc::getMessage("PARTNER_URI");
    }

    function DoInstall()
    {
        $this->InstallDB();
        $this->InstallFiles();
        ModuleManager::registerModule($this->MODULE_ID);
        $this->InstallEvents();
        $this->InstallAgent();
    }

    function DoUninstall()
    {
        $this->UnInstallDB();
        $this->UnInstallFiles();
        ModuleManager::unRegisterModule($this->MODULE_ID);
        $this->UnInstallEvents();
        $this->UnInstallAgent();
    }

    function InstallDB()
    {
        return true;
    }

    function UnInstallDB($arParams = array())
    {
        return true;
    }

    function InstallEvents()
    {
        $eventManager = EventManager::getInstance();
        $eventManager->registerEventHandler("sale", "onSaleDeliveryHandlersClassNamesBuildList", $this->MODULE_ID, "\AT\Shiptor\Handlers\Handlers", "addCustomDeliveryServices");
        $eventManager->registerEventHandler("main", "OnAdminListDisplay", $this->MODULE_ID, "\AT\Shiptor\Events", "MyOnAdminListDisplay");
        return true;
    }

    function UnInstallEvents()
    {
        $eventManager = EventManager::getInstance();
        $eventManager->unregisterEventHandler("sale", "onSaleDeliveryHandlersClassNamesBuildList", $this->MODULE_ID, "\AT\Shiptor\Handlers\Handlers", "addCustomDeliveryServices");
        $eventManager->unregisterEventHandler("main", "OnAdminListDisplay", $this->MODULE_ID, "\AT\Shiptor\Events", "MyOnAdminListDisplay");
        return true;
    }

    function InstallFiles()
    {
        return true;
    }

    function UnInstallFiles()
    {
        return true;
    }

    function InstallAgent()
    {
        \CAgent::AddAgent("\\AT\\Shiptor\\StatusPackage::checkStatusAgent();", $this->MODULE_ID, "N", 3600, "", "Y");
        return true;
    }

    function UnInstallAgent()
    {
        \CAgent::RemoveModuleAgents($this->MODULE_ID);
        return true;
    }
}
