<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main\Loader,
    AT\Shiptor\AddPackage;

if (!Loader::includeModule("at.shiptor")) {
    $arRes = [
        'error' => 'Ошибка!!! Не подключен модуль Shiptor'
    ];
    die(json_encode($arRes, JSON_UNESCAPED_UNICODE));
}

if (empty($_POST['ID'])) {
    $arRes = [
        'error' => 'Ошибка!!! Заказы не выбраны'
    ];
    die(json_encode($arRes, JSON_UNESCAPED_UNICODE));
}

$arRes = [];
foreach ($_POST['ID'] as $orderId) {
    $addPackage = new AddPackage($orderId);

    if (($resPaymentCheck = $addPackage->checkPayment()) !== null) {
        $arRes[] = $resPaymentCheck;
        continue;
    }

    if (($resStatusCheck = $addPackage->checkCorrectStatus()) !== null) {
        $arRes[] = $resStatusCheck;
        continue;
    }

    if (($resDeliveryCheck = $addPackage->checkDelivery()) !== null) {
        $arRes[] = $resDeliveryCheck;
        continue;
    }

    $arData = $addPackage->getOrderData();
    $arRes[] = $addPackage->add($arData);
}
die(json_encode($arRes, JSON_UNESCAPED_UNICODE));
