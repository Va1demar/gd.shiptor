const shiptorAddPackage = {
    add(form) {
        let data = new FormData(form);
        BX.ajax({
            url: '/local/modules/at.shiptor/ajax/add_package.php',
            data: data,
            method: 'POST',
            dataType: 'json',
            processData: false,
            preparePost: false,
            onsuccess: function (res) {
                res = JSON.parse(res);
                if (res.error) {
                    alert(res.error);
                } else {
                    $stringResult = '';
                    res.forEach(function (item, i) {
                        $stringResult += item + '\n';
                    });
                    alert($stringResult);
                }
                document.location.reload();
            },
            onfailure: e => {
                alert('Ошибка, обратитесь к администратору')
            }
        })
    }
}