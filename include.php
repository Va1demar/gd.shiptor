<?php
\Bitrix\Main\Loader::registerAutoloadClasses(
   "at.shiptor",
   array(
      '\AT\Shiptor\Handlers\Handlers' => "lib/handlers/Handlers.php",
      '\AT\Shiptor\Handlers\ProfilesHandler' => "lib/handlers/ProfilesHandler.php",
      '\AT\Shiptor\Handlers\DeliveryHandler' => "lib/handlers/DeliveryHandler.php",
      '\AT\Shiptor\Shiptor' => "lib/shiptor/Shiptor.php",
      '\AT\Shiptor\CalculateShipping' => "lib/shiptor/CalculateShipping.php",
      '\AT\Shiptor\DeliveryPoints' => "lib/shiptor/DeliveryPoints.php",
      '\AT\Shiptor\AddPackage' => "lib/shiptor/AddPackage.php",
      '\AT\Shiptor\StatusPackage' => "lib/shiptor/StatusPackage.php",
      '\AT\Shiptor\Events' => "lib/Events.php",
   )
);